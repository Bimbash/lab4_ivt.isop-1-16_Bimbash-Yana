# coding=utf-8
from django.db import models


class Contact(models.Model):
    name = models.CharField(max_length=255, verbose_name=u"Имя")
    email = models.EmailField(null=True, blank=True)
    mobile = models.CharField(max_length=255, verbose_name=u"Номер", null=True, blank=True)
    work = models.CharField(max_length=255, verbose_name=u"Рабочий номер", null=True, blank=True)


class Feedback(models.Model):
    name = models.CharField(max_length=255, verbose_name=u"Имя")
    email = models.EmailField(verbose_name=u"Email")
    message = models.TextField(verbose_name=u"Сообщение")


class Article(models.Model):
    name = models.CharField(max_length=255, verbose_name=u"Статья")

    def __str__(self):
        return self.name
