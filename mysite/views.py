# coding=utf-8
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.http import HttpResponse
from django.shortcuts import redirect, render
from django.urls import reverse
from django.views import View
from django.views.generic import TemplateView

from mysite.forms import FeedbackForm
from mysite.models import Contact, Feedback, Article


def logout_view(request):
    logout(request)
    return redirect("/")


class IndexView(TemplateView):
    template_name = "index.html"

    def dispatch(self, request, *args, **kwargs):
        request.session['name'] = "Yana Bimbash"
        return render(request,self.template_name)

    def get_context_data(self, **kwargs):
        keyword=self.request.GET.get("keyword")
        contacts=Contact.objects.all()
        if keyword:
            contacts=Contact.objects.filter(name__contains=keyword)
        context={
            'contacts': contacts
        }
        return context


class ArticlesView(TemplateView):
    template_name = "articles.html"

    def get_context_data(self, **kwargs):
        keyword=self.request.GET.get("key")
        articles=Article.objects.all()
        if keyword:
            articles = Article.objects.filter(name__contains=keyword)
        context={
            'articles': articles
        }
        return context


class Advice1View(TemplateView):
    template_name = "advice1.html"


class Advice2View(TemplateView):
    template_name = "advice2.html"


class ProfilePage(TemplateView):
    template_name = "registration/profile.html"


class RegisterView(TemplateView):
    template_name = "registration/register.html"

    def dispatch(self, request, *args, **kwargs):
        if 'username' in request.GET:
            username = request.GET['username']
            email = request.GET['email']
            password = request.GET['password']
            password2 = request.GET['password2']

            if password == password2:
                User.objects.create_user(username, email, password)

        context = {
            'name': request.session['name'],
        }
        return render (request, self.template_name,context)


class FeedbackAjaxView(View):
    def dispatch(self, request, *args, **kwargs):
        name = request.GET.get("name")
        email = request.GET.get("email")
        message = request.GET.get("message")
        if name and email and message:
            f = Feedback(name=name, email=email, message=message)
            f.save()
            return  HttpResponse(content="success")
        return  HttpResponse(content="error")


class ChatView(TemplateView):
    template_name = "chat.html"

    def dispatch(self, request, *args, **kwargs):
        context = {}
        if request.method == 'POST':
            username = request.POST.get("username")
            request.session['username'] = username

        if 'username' in request.session:
            context['username'] = request.session['username']

        return render(request,self.template_name,context)