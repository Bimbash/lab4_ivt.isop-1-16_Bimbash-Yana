"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import login

from mysite.views import IndexView, ProfilePage, logout_view, RegisterView, Advice1View, Advice2View, \
    ArticlesView, FeedbackAjaxView, ChatView

urlpatterns = [
    url(r'^accounts/login/$', login,name="login"),
    url(r'^accounts/logout/$', logout_view,name="logout"),
    url(r'^accounts/register/$', RegisterView.as_view(),name="register"),
    url(r'^accounts/profile/$', ProfilePage.as_view(),name="profile"),

    url(r'^$', IndexView.as_view(),name="index"),
    url(r'^fb/$', FeedbackAjaxView.as_view(),name="fb-ajax"),
    url(r'^advice1/$', Advice1View.as_view(),name="advice1"),
    url(r'^advice2/$', Advice2View.as_view(),name="advice2"),
    url(r'^articles/$', ArticlesView.as_view(),name="articles"),
    url(r'^chat/$', ChatView.as_view()),
    url(r'^admin/', admin.site.urls),
] + static(settings.STATIC_URL, document_root=settings.STATICFILES_DIRS)

