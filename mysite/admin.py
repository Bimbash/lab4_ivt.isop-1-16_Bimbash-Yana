from django.contrib.admin import site

from mysite.models import Feedback, Article

site.register(Feedback)
site.register(Article)